# Data Structures

Simple library with data structures based on course on Stepik platform.
It's not meant to be used, STL implementations are better in every way.

Lack of allocators, iterators and logical operators is intentional.