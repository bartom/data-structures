#include "DataTraits.hpp"

namespace DataStructures
{

template<>
const int Traits<int>::ZERO_VALUE = 0;

template<>
const double Traits<double>::ZERO_VALUE = 0.;

template<>
const std::string Traits<std::string>::ZERO_VALUE = ""s;

template<>
const int Traits<int>::CONST_VALUE = 24;

template<>
const double Traits<double>::CONST_VALUE = 4.75;

template<>
const std::string Traits<std::string>::CONST_VALUE = "test"s;

template<>
const std::initializer_list<int> Traits<int>::INIT_LIST = {2, 4, 6, 23, 42};

template<>
const std::initializer_list<double> Traits<double>::INIT_LIST = {1.2, 3.4, 4.5, 28.32, 3.2};

template<>
const std::initializer_list<std::string> Traits<std::string>::INIT_LIST =
        {"none"s, "zxc"s, "blah"s, "m"s, "end"s};

template<>
const std::initializer_list<int> Traits<int>::INIT_LIST_INSERTED_FIRST =
        {CONST_VALUE, 2, 4, 6, 23, 42};

template<>
const std::initializer_list<double> Traits<double>::INIT_LIST_INSERTED_FIRST =
        {CONST_VALUE, 1.2, 3.4, 4.5, 28.32, 3.2};

template<>
const std::initializer_list<std::string> Traits<std::string>::INIT_LIST_INSERTED_FIRST =
        {CONST_VALUE, "none"s, "zxc"s, "blah"s, "m"s, "end"s};

template<>
const std::initializer_list<int> Traits<int>::INIT_LIST_INSERTED_SECOND =
        {2, CONST_VALUE, 4, 6, 23, 42};

template<>
const std::initializer_list<double> Traits<double>::INIT_LIST_INSERTED_SECOND =
        {1.2, CONST_VALUE, 3.4, 4.5, 28.32, 3.2};

template<>
const std::initializer_list<std::string> Traits<std::string>::INIT_LIST_INSERTED_SECOND =
        {"none"s, CONST_VALUE, "zxc"s, "blah"s, "m"s, "end"s};

template<>
const std::initializer_list<int> Traits<int>::INIT_LIST_INSERTED_LAST =
        {2, 4, 6, 23, 42, CONST_VALUE};

template<>
const std::initializer_list<double> Traits<double>::INIT_LIST_INSERTED_LAST =
        {1.2, 3.4, 4.5, 28.32, 3.2, CONST_VALUE};

template<>
const std::initializer_list<std::string> Traits<std::string>::INIT_LIST_INSERTED_LAST =
        {"none"s, "zxc"s, "blah"s, "m"s, "end"s, CONST_VALUE};
}


