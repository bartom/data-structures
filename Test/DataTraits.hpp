#pragma once

#include <initializer_list>
#include <string>

namespace DataStructures
{
using namespace std::string_literals;

template <typename T>
struct Traits
{
    const static T ZERO_VALUE;
    const static T CONST_VALUE;
    const static std::initializer_list<T> INIT_LIST;
    const static std::initializer_list<T> INIT_LIST_INSERTED_FIRST;
    const static std::initializer_list<T> INIT_LIST_INSERTED_SECOND;
    const static std::initializer_list<T> INIT_LIST_INSERTED_LAST;
    constexpr static std::initializer_list<T> EMPTY_LIST = {};
};


}
