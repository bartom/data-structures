#include <gtest/gtest.h>
#include <string>
#include "LinkedList.hpp"
#include "DataTraits.hpp"

namespace DataStructures
{
using namespace ::testing;
using namespace std::string_literals;

////////////////////////////////////////////////////////////////////

namespace
{
constexpr size_t ZERO_SIZE = 0u;
constexpr size_t FIRST_INDEX = 0u;
constexpr size_t SECOND_INDEX = 1u;
constexpr size_t ARRAY_SIZE = 5u;

template <typename T>
void assertContentEqualToInitList(const LinkedList<T>& objectUnderTest, const std::initializer_list<T>& initList)
{
    ASSERT_EQ(objectUnderTest.size(), initList.size());

    size_t index = 0;
    for(const auto& elem : initList)
    {
        ASSERT_EQ(objectUnderTest.at(index), elem);
        ++index;
    }
}
}

template <typename T>
class LinkedListTestSuite : public Test
{};

using TestedTypes = Types<int, double, std::string>;

TYPED_TEST_SUITE(LinkedListTestSuite, TestedTypes);

TYPED_TEST(LinkedListTestSuite, shouldCreateEmptyLinkedList)
{
    LinkedList<TypeParam> objectUnderTest;

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), ZERO_SIZE);
}

TYPED_TEST(LinkedListTestSuite, shouldCreateZeroFilledLinkedList)
{
   LinkedList<TypeParam> objectUnderTest(ARRAY_SIZE);

   ASSERT_FALSE(objectUnderTest.empty());
   ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);

   for (size_t i = 0; i < ARRAY_SIZE; ++i)
   {
       ASSERT_EQ(objectUnderTest.at(i), Traits<TypeParam>::ZERO_VALUE);
   }
}

TYPED_TEST(LinkedListTestSuite, shouldCreateZeroFilledLinkedListWithSizeZero)
{
    LinkedList<TypeParam> objectUnderTest(ZERO_SIZE);

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), ZERO_SIZE);
}

TYPED_TEST(LinkedListTestSuite, shouldCreateConstantFilledLinkedList)
{
    LinkedList<TypeParam> objectUnderTest(ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);

    for (size_t i = 0; i < ARRAY_SIZE; ++i)
    {
        ASSERT_EQ(objectUnderTest.at(i), Traits<TypeParam>::CONST_VALUE);
    }
}

TYPED_TEST(LinkedListTestSuite, shouldCreateLinkedListFromInitializerList)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectUnderTest(initList);

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());

    assertContentEqualToInitList(objectUnderTest, initList);
}

TYPED_TEST(LinkedListTestSuite, shouldCreateLinkedListFromEmptyInitializerList)
{
    const auto& initList = Traits<TypeParam>::EMPTY_LIST;
    LinkedList<TypeParam> objectUnderTest(initList);

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());
}

TYPED_TEST(LinkedListTestSuite, shouldCopyConstructLinkedList)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectToCopyFrom = initList;
    LinkedList<TypeParam> objectUnderTest(objectToCopyFrom);

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());
    for (size_t i = 0; i < initList.size(); ++i)
    {
        ASSERT_EQ(objectUnderTest.at(i), objectToCopyFrom.at(i));
    }
}

TYPED_TEST(LinkedListTestSuite, shouldCopyConstructEmptyLinkedList)
{
    const auto& initList = Traits<TypeParam>::EMPTY_LIST;
    LinkedList<TypeParam> objectToCopyFrom = initList;
    LinkedList<TypeParam> objectUnderTest(objectToCopyFrom);

    ASSERT_TRUE(objectUnderTest.empty());
}

TYPED_TEST(LinkedListTestSuite, shouldNotPointToThisSameNodesWhenUsedCopyConstructor)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectToCopyFrom = initList;
    LinkedList<TypeParam> objectUnderTest(objectToCopyFrom);

    for (size_t i = 0; i < initList.size(); ++i)
    {
        objectToCopyFrom.at(i) = Traits<TypeParam>::CONST_VALUE;
    }

    for (size_t i = 0; i < initList.size(); ++i)
    {
        ASSERT_NE(objectUnderTest.at(i), objectToCopyFrom.at(i));
    }
}

TYPED_TEST(LinkedListTestSuite, shouldMoveConstructLinkedList)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectToMoveFrom = initList;
    LinkedList<TypeParam> objectUnderTest(std::move(objectToMoveFrom));

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());

    assertContentEqualToInitList(objectUnderTest, initList);
}

TYPED_TEST(LinkedListTestSuite, shouldAssignInitializerList)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest = initList;

    assertContentEqualToInitList(objectUnderTest, initList);
}

TYPED_TEST(LinkedListTestSuite, shouldAssignEmptyInitializerList)
{
    const auto& initList = Traits<TypeParam>::EMPTY_LIST;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest = initList;

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), 0u);
}

TYPED_TEST(LinkedListTestSuite, shouldCopyAssign)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectToCopyFrom = initList;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest = objectToCopyFrom;

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());
    for (size_t i = 0; i < initList.size(); ++i)
    {
        ASSERT_EQ(objectUnderTest.at(i), objectToCopyFrom.at(i));
    }
}

TYPED_TEST(LinkedListTestSuite, shouldMoveAssign)
{
    const auto& initList = Traits<TypeParam>::INIT_LIST;
    LinkedList<TypeParam> objectToMoveFrom = initList;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest = std::move(objectToMoveFrom);

    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), initList.size());

    assertContentEqualToInitList(objectUnderTest, initList);
}

TYPED_TEST(LinkedListTestSuite, shouldInsertIntoEmptyList)
{
    constexpr size_t SIZE_AFTER_INSERT = 1u;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest.insert(FIRST_INDEX, Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(objectUnderTest.size(), SIZE_AFTER_INSERT);
    ASSERT_EQ(objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(LinkedListTestSuite, shouldEraseFirst)
{
    LinkedList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_FIRST;

    objectUnderTest.erase(FIRST_INDEX);
    ASSERT_EQ(objectUnderTest.front(), *(Traits<TypeParam>::INIT_LIST.begin()));
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);

}

TYPED_TEST(LinkedListTestSuite, shouldEraseLast)
{
    LinkedList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_LAST;
    const size_t LAST_INDEX = objectUnderTest.size()-1;

    objectUnderTest.erase(LAST_INDEX);
    auto iter = Traits<TypeParam>::INIT_LIST.end();
    --iter;
    ASSERT_EQ(objectUnderTest.back(), *iter);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);

}

TYPED_TEST(LinkedListTestSuite, shouldEraseSecond)
{
    LinkedList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_SECOND;
    objectUnderTest.erase(SECOND_INDEX);

    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(LinkedListTestSuite, shouldEraseOnlyElement)
{
    LinkedList<TypeParam> objectUnderTest(1u, Traits<TypeParam>::CONST_VALUE);
    objectUnderTest.erase(FIRST_INDEX);

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), 0u);

}

TYPED_TEST(LinkedListTestSuite, shouldPushBackToEmptyList)
{
    constexpr size_t SIZE_AFTER_INSERT = 1u;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(objectUnderTest.size(), SIZE_AFTER_INSERT);
    ASSERT_EQ(objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(LinkedListTestSuite, shouldPopBackSingleElement)
{
    constexpr size_t INITIAL_SIZE = 1u;
    LinkedList<TypeParam> objectUnderTest(INITIAL_SIZE);

    objectUnderTest.pop_back();
    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), 0u);
}

TYPED_TEST(LinkedListTestSuite, shouldPopBack)
{
    LinkedList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_LAST;

    objectUnderTest.pop_back();
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(LinkedListTestSuite, shouldPushFrontToEmptyList)
{
    constexpr size_t SIZE_AFTER_INSERT = 1u;
    LinkedList<TypeParam> objectUnderTest;

    objectUnderTest.push_front(Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(objectUnderTest.size(), SIZE_AFTER_INSERT);
    ASSERT_EQ(objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
}

template <typename T>
class LinkedListInitializedTestSuite : public LinkedListTestSuite<T>
{
public:
    LinkedListInitializedTestSuite():
        m_initList(Traits<T>::INIT_LIST),
        m_objectUnderTest(m_initList)
    {}

protected:
    const std::initializer_list<T>& m_initList;
    LinkedList<T> m_objectUnderTest;
};

TYPED_TEST_SUITE(LinkedListInitializedTestSuite, TestedTypes);

TYPED_TEST(LinkedListInitializedTestSuite, shouldGetCorrectElementAtPosition)
{
    assertContentEqualToInitList(this->m_objectUnderTest, this->m_initList);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldGetFrontElement)
{
    ASSERT_EQ(this->m_objectUnderTest.front(), *(this->m_initList.begin()));
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldModifyFrontElement)
{
    this->m_objectUnderTest.front() = Traits<TypeParam>::CONST_VALUE;
    ASSERT_EQ(this->m_objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldGetBackElement)
{
    auto iter = this->m_initList.end();
    --iter;
    ASSERT_EQ(this->m_objectUnderTest.back(), *iter);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldModifyBackElement)
{
    this->m_objectUnderTest.back() = Traits<TypeParam>::CONST_VALUE;
    ASSERT_EQ(this->m_objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldBeEmptyWhenCleared)
{
    this->m_objectUnderTest.clear();
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::EMPTY_LIST);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldInsertFirstElement)
{
    this->m_objectUnderTest.insert(FIRST_INDEX, Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(this->m_objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_FIRST);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldInsertLastElement)
{
    this->m_objectUnderTest.insert(ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(this->m_objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_LAST);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldInsertSecondElement)
{
    this->m_objectUnderTest.insert(SECOND_INDEX, Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(this->m_objectUnderTest.at(SECOND_INDEX), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_SECOND);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldPushBack)
{
    this->m_objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(this->m_objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_LAST);
}

TYPED_TEST(LinkedListInitializedTestSuite, shouldPushFront)
{
    this->m_objectUnderTest.push_front(Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(this->m_objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_FIRST);
}

// TODO
// exception tests
// resize test
// swap test
// contains test

}
