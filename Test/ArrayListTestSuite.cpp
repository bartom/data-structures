#include <gtest/gtest.h>
#include <string>
#include "ArrayList.hpp"
#include "DataTraits.hpp"

namespace DataStructures
{
using namespace ::testing;
using namespace std::string_literals;

namespace
{
////////////////////////////////////////////////////////////////////

constexpr size_t ARRAY_SIZE = 5u;
constexpr size_t RESIZED_SIZE = 10u;
constexpr size_t CHANGED_INDEX = 1u;
constexpr size_t FIRST_INDEX = 0u;

////////////////////////////////////////////////////////////////////

template <typename T>
void assertCorrectSize(const ArrayList<T>& objectUnderTest)
{
    ASSERT_FALSE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(objectUnderTest.capacity(), ARRAY_SIZE);
}

template <typename T>
void assertContentEqualToSingleValue(const ArrayList<T>& objectUnderTest, T value)
{
    auto* data = objectUnderTest.data();
    ASSERT_NE(data, nullptr);

    for (size_t i = 0; i < objectUnderTest.size(); ++i)
    {
        ASSERT_EQ(data[i], value);
        ASSERT_EQ(objectUnderTest[i], value);
        ASSERT_EQ(objectUnderTest.at(i), value);
    }
}

template <typename T>
void assertContentEqualToInitList(const ArrayList<T>& objectUnderTest, const std::initializer_list<T>& initList)
{
    auto* data = objectUnderTest.data();
    ASSERT_NE(data, nullptr);

    size_t index = 0;
    for(const auto& elem : initList)
    {
        ASSERT_EQ(data[index], elem);
        ASSERT_EQ(objectUnderTest[index], elem);
        ASSERT_EQ(objectUnderTest.at(index), elem);
        ++index;
    }
}

template <typename T>
void assertContentOfTwoListsEqual(const ArrayList<T>& objectUnderTest, const ArrayList<T>& other)
{
    auto* data = objectUnderTest.data();
    ASSERT_NE(data, nullptr);

    for (size_t i = 0; i < objectUnderTest.size(); ++i)
    {
        ASSERT_EQ(data[i], other[i]);
        ASSERT_EQ(objectUnderTest[i], other[i]);
        ASSERT_EQ(objectUnderTest.at(i), other[i]);
    }
}

}

template <typename T>
class ArrayListTestSuite : public Test
{};

using TestedTypes = Types<int, double, std::string>;

TYPED_TEST_SUITE(ArrayListTestSuite, TestedTypes);

TYPED_TEST(ArrayListTestSuite, shouldCreateEmptyArrayList)
{
    ArrayList<TypeParam> objectUnderTest;

    ASSERT_TRUE(objectUnderTest.empty());
    ASSERT_EQ(objectUnderTest.size(), 0u);
    ASSERT_EQ(objectUnderTest.capacity(), 0u);
    ASSERT_EQ(objectUnderTest.data(), nullptr);
}

TYPED_TEST(ArrayListTestSuite, shouldCreateZeroFilledArrayList)
{
    ArrayList<TypeParam> objectUnderTest(ARRAY_SIZE);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToSingleValue(objectUnderTest, Traits<TypeParam>::ZERO_VALUE);
}

TYPED_TEST(ArrayListTestSuite, shouldCreateValueFilledArrayList)
{
    ArrayList<TypeParam> objectUnderTest(ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToSingleValue(objectUnderTest, Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListTestSuite, shouldCreateArrayListFromInitList)
{
    ArrayList<TypeParam> objectUnderTest(Traits<TypeParam>::INIT_LIST);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldCreateArrayListCopy)
{
    const ArrayList<TypeParam> objectToCopyFrom = Traits<TypeParam>::INIT_LIST;
    ArrayList<TypeParam> objectUnderTest(objectToCopyFrom);

    assertCorrectSize(objectUnderTest);
    assertContentOfTwoListsEqual(objectUnderTest, objectToCopyFrom);
}

TYPED_TEST(ArrayListTestSuite, shouldCreateArrayListFromMove)
{
    ArrayList<TypeParam> objectToMoveFrom = Traits<TypeParam>::INIT_LIST;
    ArrayList<TypeParam> objectUnderTest(std::move(objectToMoveFrom));

    assertCorrectSize(objectUnderTest);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);

    ASSERT_EQ(objectToMoveFrom.data(), nullptr);
}

TYPED_TEST(ArrayListTestSuite, shouldAssignFromCopy)
{
    const ArrayList<TypeParam> objectToCopyFrom = Traits<TypeParam>::INIT_LIST;
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest = objectToCopyFrom;

    assertCorrectSize(objectUnderTest);
    assertContentOfTwoListsEqual(objectUnderTest, objectToCopyFrom);
}

TYPED_TEST(ArrayListTestSuite, shouldMoveAssign)
{
    ArrayList<TypeParam> objectToMoveFrom = Traits<TypeParam>::INIT_LIST;
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest = std::move(objectToMoveFrom);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);

    ASSERT_EQ(objectToMoveFrom.data(), nullptr);
}

TYPED_TEST(ArrayListTestSuite, shouldAssignFromInitList)
{
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest = Traits<TypeParam>::INIT_LIST;

    assertCorrectSize(objectUnderTest);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldAssignSingleValue)
{
    ArrayList<TypeParam> objectUnderTest;
    objectUnderTest.assign(ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToSingleValue(objectUnderTest, Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListTestSuite, shouldAssignWithInitList)
{
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest.assign(Traits<TypeParam>::INIT_LIST);

    assertCorrectSize(objectUnderTest);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldInsertWhenSizeIsZero)
{
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest.insert(FIRST_INDEX, Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(objectUnderTest.size(), 1u);
    ASSERT_EQ(objectUnderTest.capacity(), 1u);
    ASSERT_EQ(objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListTestSuite, shouldPushBackWhenSizeIsZero)
{
    ArrayList<TypeParam> objectUnderTest;

    objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(objectUnderTest.size(), 1u);
    ASSERT_EQ(objectUnderTest.capacity(), 1u);
    ASSERT_EQ(objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
    ASSERT_EQ(objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListTestSuite, shouldThrowWhenAttemptedEraseOnEmptyContainer)
{
    ArrayList<TypeParam> objectUnderTest;

    ASSERT_THROW(objectUnderTest.erase(FIRST_INDEX), std::out_of_range);
}

TYPED_TEST(ArrayListTestSuite, shouldEraseSecondElement)
{
    ArrayList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_SECOND;

    objectUnderTest.erase(CHANGED_INDEX);

    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(objectUnderTest.capacity(), ARRAY_SIZE+1);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldEraseFirstElement)
{
    ArrayList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_FIRST;

    objectUnderTest.erase(FIRST_INDEX);

    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(objectUnderTest.capacity(), ARRAY_SIZE+1);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldEraseLastElement)
{
    ArrayList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_LAST;

    objectUnderTest.erase(ARRAY_SIZE);

    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(objectUnderTest.capacity(), ARRAY_SIZE+1);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldEraseOnlyElement)
{
    ArrayList<TypeParam> objectUnderTest(1u, Traits<TypeParam>::CONST_VALUE);

    objectUnderTest.erase(FIRST_INDEX);
    ASSERT_EQ(objectUnderTest.size(), 0u);
    ASSERT_EQ(objectUnderTest.capacity(), 1u);
}

TYPED_TEST(ArrayListTestSuite, shouldPopBackLastElement)
{
    ArrayList<TypeParam> objectUnderTest = Traits<TypeParam>::INIT_LIST_INSERTED_LAST;

    objectUnderTest.pop_back();

    ASSERT_EQ(objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(objectUnderTest.capacity(), ARRAY_SIZE+1);
    assertContentEqualToInitList(objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListTestSuite, shouldPopBackOnlyElement)
{
    ArrayList<TypeParam> objectUnderTest(1u, Traits<TypeParam>::CONST_VALUE);

    objectUnderTest.pop_back();
    ASSERT_EQ(objectUnderTest.size(), 0u);
    ASSERT_EQ(objectUnderTest.capacity(), 1u);
}

template <typename T>
class ArrayListInitializedTestSuite : public ArrayListTestSuite<T>
{
public:
    ArrayListInitializedTestSuite():
        m_objectUnderTest(Traits<T>::INIT_LIST)
    {}

protected:
    ArrayList<T> m_objectUnderTest;
};

TYPED_TEST_SUITE(ArrayListInitializedTestSuite, TestedTypes);

TYPED_TEST(ArrayListInitializedTestSuite, shoulThrowExceptionWhenAtCalledWithWrongIndex)
{
    ASSERT_THROW(this->m_objectUnderTest.at(ARRAY_SIZE), std::out_of_range);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldAssignSingleValueWithAtMethod)
{
    this->m_objectUnderTest.at(CHANGED_INDEX) = Traits<TypeParam>::CONST_VALUE;

    ASSERT_EQ(this->m_objectUnderTest[CHANGED_INDEX], Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldAssignSingleValueWithBracketOperator)
{
    this->m_objectUnderTest[CHANGED_INDEX] = Traits<TypeParam>::CONST_VALUE;

    ASSERT_EQ(this->m_objectUnderTest.at(CHANGED_INDEX), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldGetFrontValue)
{
    ASSERT_EQ(this->m_objectUnderTest.front(), *Traits<TypeParam>::INIT_LIST.begin());
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldGetBackValue)
{
    auto iter = Traits<TypeParam>::INIT_LIST.end();
    --iter;
    ASSERT_EQ(this->m_objectUnderTest.back(), *iter);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldChangeFrontValue)
{
    this->m_objectUnderTest.front() = Traits<TypeParam>::CONST_VALUE;
    ASSERT_EQ(this->m_objectUnderTest.front(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldChangeBackValue)
{
    this->m_objectUnderTest.back() = Traits<TypeParam>::CONST_VALUE;
    ASSERT_EQ(this->m_objectUnderTest.back(), Traits<TypeParam>::CONST_VALUE);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldReserveArrayList)
{
    this->m_objectUnderTest.reserve(RESIZED_SIZE);

    ASSERT_EQ(this->m_objectUnderTest.size(), ARRAY_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), RESIZED_SIZE);

    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldDoNothingWhenReserveSizeLessThanCapacity)
{
    this->m_objectUnderTest.reserve(2u);

    assertCorrectSize(this->m_objectUnderTest);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldShrinkToSize)
{
    this->m_objectUnderTest.reserve(RESIZED_SIZE);
    this->m_objectUnderTest.shrink_to_fit();

    assertCorrectSize(this->m_objectUnderTest);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldNotShrinkIfCapacityEqualToSize)
{
    this->m_objectUnderTest.shrink_to_fit();

    assertCorrectSize(this->m_objectUnderTest);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldClearContent)
{
    this->m_objectUnderTest.clear();

    ASSERT_TRUE(this->m_objectUnderTest.empty());
    ASSERT_EQ(this->m_objectUnderTest.size(), 0u);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), ARRAY_SIZE);
    ASSERT_NE(this->m_objectUnderTest.data(), nullptr);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldNotInsertWhenIndexIsAfterSize)
{
    ASSERT_THROW(
        this->m_objectUnderTest.insert(ARRAY_SIZE+1, Traits<TypeParam>::CONST_VALUE),
        std::out_of_range);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldInsertWhenSizeEqualsCapacity)
{
    this->m_objectUnderTest.insert(CHANGED_INDEX, Traits<TypeParam>::CONST_VALUE);

    constexpr auto CURRENT_SIZE = ARRAY_SIZE+1;
    ASSERT_EQ(this->m_objectUnderTest.size(), CURRENT_SIZE);

    constexpr auto CURRENT_CAPACITY = ARRAY_SIZE*2;
    ASSERT_EQ(this->m_objectUnderTest.capacity(), CURRENT_CAPACITY);

    ASSERT_EQ(this->m_objectUnderTest.at(CHANGED_INDEX), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_SECOND);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldInsertLastElementWhenCapacityNotReached)
{
    constexpr auto NEW_CAPACITY = ARRAY_SIZE+3;

    this->m_objectUnderTest.reserve(NEW_CAPACITY);
    this->m_objectUnderTest.insert(ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);

    constexpr auto CURRENT_SIZE = ARRAY_SIZE+1;
    ASSERT_EQ(this->m_objectUnderTest.size(), CURRENT_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), NEW_CAPACITY);

    ASSERT_EQ(this->m_objectUnderTest.at(ARRAY_SIZE), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_LAST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldInsertFirstElementWhenCapacityNotReached)
{
    constexpr auto NEW_CAPACITY = ARRAY_SIZE+3;

    this->m_objectUnderTest.reserve(NEW_CAPACITY);
    this->m_objectUnderTest.insert(FIRST_INDEX, Traits<TypeParam>::CONST_VALUE);

    constexpr auto CURRENT_SIZE = ARRAY_SIZE+1;
    ASSERT_EQ(this->m_objectUnderTest.size(), CURRENT_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), NEW_CAPACITY);

    ASSERT_EQ(this->m_objectUnderTest.at(FIRST_INDEX), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_FIRST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldNotContainBeforeInsert)
{
   ASSERT_FALSE(this->m_objectUnderTest.contains(Traits<TypeParam>::CONST_VALUE));
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldPushBackToExistingContainer)
{
    this->m_objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);

    constexpr auto CURRENT_SIZE = ARRAY_SIZE+1;
    ASSERT_EQ(this->m_objectUnderTest.size(), CURRENT_SIZE);

    constexpr auto CURRENT_CAPACITY = ARRAY_SIZE*2;
    ASSERT_EQ(this->m_objectUnderTest.capacity(), CURRENT_CAPACITY);

    ASSERT_EQ(this->m_objectUnderTest.at(ARRAY_SIZE), Traits<TypeParam>::CONST_VALUE);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_LAST);

}

TYPED_TEST(ArrayListInitializedTestSuite, shouldContainAfterPush)
{
    this->m_objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);
    ASSERT_TRUE(this->m_objectUnderTest.contains(Traits<TypeParam>::CONST_VALUE));
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldSwapContent)
{
    ArrayList<TypeParam> other = Traits<TypeParam>::INIT_LIST_INSERTED_SECOND;
    this->m_objectUnderTest.swap(other);

    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST_INSERTED_SECOND);
    assertContentEqualToInitList(other, Traits<TypeParam>::INIT_LIST);
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldNotResizeWhenCountEqualsSize)
{
    this->m_objectUnderTest.resize(ARRAY_SIZE);

    assertCorrectSize(this->m_objectUnderTest);
    assertContentEqualToInitList(this->m_objectUnderTest, Traits<TypeParam>::INIT_LIST);

}

TYPED_TEST(ArrayListInitializedTestSuite, shouldResizeDown)
{
    constexpr size_t NEW_ARRAY_SIZE = ARRAY_SIZE - 3;
    this->m_objectUnderTest.resize(NEW_ARRAY_SIZE);

    ASSERT_EQ(this->m_objectUnderTest.size(), NEW_ARRAY_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), ARRAY_SIZE);

    size_t index = 0;
    for(const auto& elem : Traits<TypeParam>::INIT_LIST)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), elem);
        ++index;
        if (index == NEW_ARRAY_SIZE) break;
    }
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldResizeUpAndEnlargeCapacity)
{
    constexpr size_t NEW_ARRAY_SIZE = ARRAY_SIZE + 4;
    this->m_objectUnderTest.resize(NEW_ARRAY_SIZE);

    ASSERT_EQ(this->m_objectUnderTest.size(), NEW_ARRAY_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), NEW_ARRAY_SIZE);

    size_t index = 0;
    for(const auto& elem : Traits<TypeParam>::INIT_LIST)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), elem);
        ++index;
    }

    for(; index < NEW_ARRAY_SIZE; ++index)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), Traits<TypeParam>::ZERO_VALUE);
    }
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldResizeUpAndNotEnlargeCapacity)
{

    constexpr size_t NEW_ARRAY_CAPACITY = ARRAY_SIZE * 4;
    constexpr size_t NEW_ARRAY_SIZE = ARRAY_SIZE + 4;
    this->m_objectUnderTest.reserve(NEW_ARRAY_CAPACITY);
    this->m_objectUnderTest.resize(NEW_ARRAY_SIZE, Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(this->m_objectUnderTest.size(), NEW_ARRAY_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), NEW_ARRAY_CAPACITY);

    size_t index = 0;
    for(const auto& elem : Traits<TypeParam>::INIT_LIST)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), elem);
        ++index;
    }

    for(; index < NEW_ARRAY_SIZE; ++index)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), Traits<TypeParam>::CONST_VALUE);
    }
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldResizeDownAndUp)
{
    constexpr size_t MID_SIZE = ARRAY_SIZE - 3;
    constexpr size_t END_SIZE = ARRAY_SIZE + 8;

    this->m_objectUnderTest.resize(MID_SIZE);
    this->m_objectUnderTest.resize(END_SIZE);

    ASSERT_EQ(this->m_objectUnderTest.size(), END_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), END_SIZE);

    size_t index = 0;
    for(const auto& elem : Traits<TypeParam>::INIT_LIST)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), elem);
        ++index;
        if (index == MID_SIZE) break;
    }

    for(; index < END_SIZE; ++index)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), Traits<TypeParam>::ZERO_VALUE);
    }
}

TYPED_TEST(ArrayListInitializedTestSuite, shouldResizeDownAndPushBack)
{
    constexpr size_t MID_SIZE = ARRAY_SIZE - 3;
    constexpr size_t END_SIZE = MID_SIZE + 1;

    this->m_objectUnderTest.resize(MID_SIZE);
    this->m_objectUnderTest.push_back(Traits<TypeParam>::CONST_VALUE);

    ASSERT_EQ(this->m_objectUnderTest.size(), END_SIZE);
    ASSERT_EQ(this->m_objectUnderTest.capacity(), ARRAY_SIZE);

    size_t index = 0;
    for(const auto& elem : Traits<TypeParam>::INIT_LIST)
    {
        ASSERT_EQ(this->m_objectUnderTest.at(index), elem);
        ++index;
        if (index == MID_SIZE) break;
    }

    ASSERT_EQ(this->m_objectUnderTest.at(END_SIZE-1), Traits<TypeParam>::CONST_VALUE);
}


}


